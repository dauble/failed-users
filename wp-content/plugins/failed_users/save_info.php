<?php

  require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-config.php');
  require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-includes/wp-db.php');

  global $wpdb;
  date_default_timezone_set('US/East-Indiana');

  // store $_POST into variables
  $first_name = $_POST['first_name'];
  $last_name = $_POST['last_name'];
  $email = $_POST['email'];
  $phone = $_POST['phone'];
  $zip = $_POST['zip'];
  $education = str_replace("'", "''", $_POST['education']);
  $site_of_interest = $_POST['location'];
  $referral_url = $_POST['referral_url'];

  $date = date('Y-m-d H:i:s');
  $date_est = mktime(date('H')-6, date('i'), date('s'), date("m"), date("d"), date("Y"));
  $today = date($date, $date_est);

  // insert values into db via WordPress
  $wp_query = array(
    'first_name' => $first_name,
    'last_name' => $last_name,
    'email' => $email,
    'phone' => $phone,
    'zip' => $zip,
    'education_completed' => $education,
    'referral_url' => $referral_url,
    'date_submitted' => $today
  );

  $wpdb->insert('wp_failed_users', $wp_query);

?>