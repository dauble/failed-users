<?php

  /*
  Plugin Name: Failed Users
  Description: View failed users on Salesforce lead forms
  Version: 2.0
  Author: David M. Auble
  Author URI: http://www.orbiseducation.com
  */

  // init jquery
  wp_enqueue_script('jquery');
  // init custom scripts
  wp_register_script('jquery_validate', 'http://cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js');
  wp_enqueue_script('jquery_validate');

  // init custom scripts
  wp_register_style( 'failed_users', plugins_url('failed_users/css/styles.css'));
  wp_enqueue_style('failed_users');

  // init custom scripts
  wp_register_script('failed_users', plugins_url('failed_users/js/scripts.js'));
  wp_enqueue_script('failed_users');

  // Initialize the plugin and add the menu
  add_action( 'admin_menu', 'fu_add_admin_menu' );

  // Add an Admin Menu option for Settings
  function fu_add_admin_menu(  ) {
    // add_options_page( 'Failed Users', 'Failed Users', 'manage_options', 'failed_users', 'failed_users_options_page' );
    add_utility_page( 'Failed Users', 'Failed Users', 'manage_options', 'failed_users', 'failed_users_options_page', 'dashicons-groups' );
  }

  // Build the Administrative form to save the default amount
  function failed_users_options_page() {
    global $wpdb;

    // check to see if table exisits, if not, create table 'wp_failed_users'
    $check_tables = $wpdb->get_var("SHOW TABLES LIKE 'wp_failed_users'");
    if($check_tables == '') {
      $sql = "CREATE TABLE `wp_failed_users` (
          `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
          `first_name` VARCHAR(45),
          `last_name` VARCHAR(45),
          `email` VARCHAR(100),
          `phone` VARCHAR(20),
          `zip` VARCHAR(5),
          `education_completed` VARCHAR(45),
          `referral_url` VARCHAR(300) NULL,
          `date_submitted` DATETIME
        ) DEFAULT CHARSET=utf8;";

      $wpdb->query($sql);
    }

    $mysql_query = "SELECT * FROM wp_failed_users ORDER BY id DESC";
    $results = $wpdb->get_results($mysql_query);
?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <h1>Failed Users</h1>
          <p>These users have filled out a lead form, yet did not qualify based on their initial educational background. These leads have changed their educational background in order to pre-qualify.</p>

          <h2>Export List</h2>
          <form action="/wp-content/plugins/failed_users/export.php" enctype="multipart/form-data" method="post">
            <input type="submit" class="btn btn-primary" value="Export to CSV">
          </form>
          <br>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Zip Code</th>
                <th>Education Level</th>
                <th>Referral URL</th>
                <th>Date Submitted</th>
              </tr>
            </thead>
            <tbody>
              <?php if($results): ?>
                <?php foreach($results as $row): ?>
                  <?php $date = date("n/d/Y h:ia", strtotime($row->date_submitted)); ?>
                  <tr>
                    <th scope="row"><?php echo $row->id; ?></th>
                    <td><?php echo $row->first_name; ?></td>
                    <td><?php echo $row->last_name; ?></td>
                    <td><a href="mailto:<?php echo $row->email; ?>"><?php echo $row->email; ?></a></td>
                    <td><?php echo $row->phone; ?></td>
                    <td><?php echo $row->zip; ?></td>
                    <td><?php echo $row->education_completed; ?></td>
                    <td><?php echo $row->referral_url; ?></td>
                    <td><?php echo $date; ?></td>
                  </tr>
                <?php endforeach; ?>
              <?php else: ?>
                <tr>
                  <td colspan="8" class="text-center">There are no records to display</td>
                </tr>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <h2>Export List</h2>
          <form action="<?php dirname(__FILE__); ?>/export.php" enctype="multipart/form-data" method="post">
            <input type="submit" class="btn btn-primary" value="Export to CSV">
          </form><br><br>
        </div>
      </div>
    </div>
  <?php } ?>