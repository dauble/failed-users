<?php

  require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-config.php' );
  require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-includes/wp-db.php' );

  global $wpdb;
  $results = $wpdb->get_results("SELECT * FROM wp_failed_users", ARRAY_A);

  // echo "<pre>";
  // print_r($results);
  // echo "</pre>";

  if ($results != null) {
    // start exporting data
    $file = "failed_users_" . date('Ymd') . ".csv";
    header( "Content-Type: text/csv;charset=utf-8" );
    header( "Content-Disposition: attachment;filename=\"$file\"" );
    header("Pragma: no-cache");
    header("Expires: 0");

    $fp = fopen("php://output", "w");

    $counter = 0;
    foreach($results as $key => $value) {
      // echo "<pre>";
      // print_r($result);
      // echo "</pre>";

      //save header
      if($counter == 0)
        fputcsv($fp, ucwords(str_replace("_"," ",$key));  //clean up field names: replace _ with space and cap first letter of each word

      fputcsv($fp, $value);

      $counter++;
    }

    fclose($fp);

    return true;
  } else {
    // error with query
    echo "error";
    return false;
  }

?>