$ = jQuery.noConflict();
$(document).ready(function() {
  if(typeof $.validate == "function"){
    $.formUtils.addValidator({
      name: "zip_code",
      validatorFunction: function(value, $el, config, language, $form) {
        var zipArray = new Array("00000", "11111", "22222", "33333", "44444", "55555", "66666", "77777", "88888", "99999", "12345", "32132", "98765");

        if(!zipArray.includes(value)) {
          var zipRegex = new RegExp("^\\d{5}(-\\d{4})?$");

          return zipRegex.test(value);
        } else {
          return false;
        }
      },
      onError: function() {
        RecordFailure();
      },
      errorMessage: "This is not a valid zip code",
      errrorMessageKey: "badZip"
    });

    $.validate({
      onError: function() {
        RecordFailure();
      }
    });
  }
});

function RecordFailure() {
  var $form_select = $('#00NC00000067ygh').val();

  $.post("/wp-content/plugins/failed_users/save_info.php", {
    first_name: $("#first_name").val(),
    last_name: $("#last_name").val(),
    email: $("#email").val(),
    phone: $("#phone").val(),
    zip: $("#zip").val(),
    education: $("#00NC00000067ygh").val(),
    referral_url: window.location.href,
    location: $("#00NC00000068kmS").val()
  }, function() {
    // successful post
    console.log("success");
  }).error(function() {
    // unsuccessful post
    console.log("fail");
  });
}