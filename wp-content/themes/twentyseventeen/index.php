<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<?php if ( is_home() && ! is_front_page() ) : ?>
		<header class="page-header">
			<h1 class="page-title"><?php single_post_title(); ?></h1>
		</header>
	<?php else : ?>
	<header class="page-header">
		<h2 class="page-title"><?php _e( 'Posts', 'twentyseventeen' ); ?></h2>
	</header>
	<?php endif; ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

      <!-- insert lead form -->
      <form action="https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST" class="custform has-validation-callback" id="customForm">
        <div class="container-fluid">
          <?php
            $site_location = explode("/", $_SERVER['REQUEST_URI']);
            $page_url .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
          ?>

          <input type="hidden" name="retURL" value="http://onlineabsn.marian.edu/indianapolis/contact/thank-you-for-contact-us/" >
          <input type="hidden" name="oid" value="00DC0000000yMOY">
          <input type="hidden" name="00NC000000688xY" id="00NC000000688xY" value="INT-Website">
          <input type="hidden" name="00NC000000688xX" id="00NC000000688xX" value="INT-MarianNursing.com">
          <input type="hidden" name="00NC00000068kmS" id="00NC00000068kmS" value="a0KC000000F0uugMAB">
          <input type="hidden" name="00NC00000068kmX" id="00NC00000068kmX" value="001C000001HpenfIAB">
          <input type="hidden" name="00NC000000688xW" id="00NC000000688xW" value="Web Form">
          <input type="hidden" name="00NC00000067ygp" id="00NC00000067ygp" value="<?php echo $page_url; ?>">

          <div class="row">
            <div class="form-group col-sm-6">
              <label for="first_name" class="sr-only"></label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                <input type="text" class="form-control" name="first_name" id="first_name" value="" placeholder="First Name" data-validation="length" data-validation-length="2-25">
              </div>
            </div>

            <div class="form-group col-sm-6">
              <label for="last_name" class="sr-only"></label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                <input type="text" class="form-control" name="last_name" id="last_name" value="" placeholder="Last Name" data-validation="length" data-validation-length="2-50">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group">
              <label for="email" class="sr-only"></label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                <input type="email" class="form-control" name="email" id="email" value="" placeholder="Email" data-validation="email">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group">
              <label for="phone" class="sr-only"></label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                <input type="tel" class="form-control" name="phone" id="phone" value="" placeholder="Phone" data-validation="length" data-validation-length="14">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group">
              <label for="zip" class="sr-only"></label>
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                <input type="tel" class="form-control" name="zip" id="zip" value="" placeholder="Zip Code" data-validation="zip_code" data-validation-length="5">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group">
              <label for="00NC00000067ygh" class="sr-only"></label>
              <select id="00NC00000067ygh" class="form-control" name="00NC00000067ygh" title="Highest Level of Education Completed" data-validation="required">
                <option value="">Highest Level of Education Completed:</option>
                <option value="HS/GED">HS/GED</option>
                <option value="Some College (0 to 34 credits)">Some College (0 to 34 credits)</option>
                <option value="Some College (35 to 59 credits)">Some College (35 to 59 credits)</option>
                <option value="Some College (60 to 120 credits)">Some College (60 to 120 credits)</option>
                <option value="Diploma in Nursing">Diploma in Nursing</option>
                <option value="Associate's Degree - Non-Nursing">Associate's Degree - Non-Nursing</option>
                <option value="Associate's Degree - Nursing">Associate's Degree - Nursing</option>
                <option value="Bachelor's Degree - Non-Nursing">Bachelor's Degree - Non-Nursing</option>
                <option value="Bachelor's Degree - Nursing">Bachelor's Degree - Nursing</option>
                <option value="Master's Degree - Non-Nursing">Master's Degree - Non-Nursing</option>
                <option value="Master's Degree - Nursing">Master's Degree - Nursing</option>
              </select>
            </div>
          </div>

          <div class="row">
            <div class="form-group">
              <div class="input-group">
                <p class="text-sm text-center"><i>Clicking the "Get Started"  button constitutes your express written consent to be emailed, called and/or texted by Marian University at the numbers you provided (including a wireless number if provided) regarding furthering your education. You understand that these contacts may be generated using automated technology and that you are not required to give this consent to enroll in programs with the school.</i></p>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="form-group">
              <input type="submit" name="submit" value="Get Started" class="btn btn-primary">
            </div>
          </div>
        </div>
      </form>
      <!-- end lead form -->

			<?php
			if ( have_posts() ) :

				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/post/content', get_post_format() );

				endwhile;

				the_posts_pagination( array(
					'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
					'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
					'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
				) );

			else :

				get_template_part( 'template-parts/post/content', 'none' );

			endif;
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
