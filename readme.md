# Failed Users #

This is a simple plugin designed to capture data entered in forms if someone does not qualify initially. The purpose is to test if users are changing answers to become qualified.

Works with WordPress and Salesforce.

### Requirements ###

* WordPress 4.x
* jQuery
* [jQuery Form Validator](https://github.com/victorjonsson/jQuery-Form-Validator) (must be installed on your site)

### To Do: ###

* Drag and drop `failed_users` folder into your plugins folder
* Activate plugin like you would normally

### Who do I talk to? ###

* MIT license, Twitter: @dauble