<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'failedusers');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'j+V|fL-N( SidmJDMV=ALjwx]oJD2L+ @@2@Au3};|s}1P(V9K&-smz4h9npCN#@');
define('SECURE_AUTH_KEY',  'e]7rD=^XtnOTxw-li}pXHXA47h|{FU,uk+cu- shdkpwr=D0}ZcvX/CU+Vt`U~_P');
define('LOGGED_IN_KEY',    'S&5sHzCpB_hzbNb)HEFmsO4j;SZLnadMp7m4Ok:MeY)CbO>36p!5H5_[R%yt1G+Q');
define('NONCE_KEY',        'jfrV)y,[n=?w2*fboFT(gD($WlRf~~!zeY<F&mKr3:xCLsZ,+:BM*m!rwR+w_O%+');
define('AUTH_SALT',        'gN&x3(=O}YOu6-S!zrQW,|SQp87DOzSV?j+_cy)4xIqL4}Njy%jO4T-DF1Pw]JA|');
define('SECURE_AUTH_SALT', '5bn8`P-4f(xxTb*[xt v_+cfTaSb3VEI+*,SAnY,nhC#v*)+Bt;GwxK742))/WA+');
define('LOGGED_IN_SALT',   'pl!b4$GOLuqAsXKqh<=<:?|n4qQYbFmDlUnF-?5xBo1H-_Gy+:[4XjI R1G|$dJe');
define('NONCE_SALT',       '<9qal_QWxFUHTXy5KWdNil^iZO?hxWg (;ZOCtCqm2S.^&K!4u2XVBJ$b6y[`|DI');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
